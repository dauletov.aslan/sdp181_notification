package kz.astana.notificationexample;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

public class MainActivity extends AppCompatActivity {

    private final String NOTIFICATION_CHANNEL = "NOTIFICATION_CHANNEL";
    private final int REQUEST_CODE = 300;
    private int NOTIFY_ID = 100;
    private int NOTIFY_WITH_INTENT_ID = 200;
    private int NOTIFY_CUSTOM_ID = 400;
    private NotificationManager notificationManager;

    private String[] titles = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth"};
    private String[] notifications = {
            "First notification",
            "Second notification",
            "Third notification",
            "Fourth notification",
            "Fifth notification",
            "Sixth notification",
            "Seventh notification",
            "Eighth notification",
            "Ninth notification",
            "Tenth notification"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();

        Button createNotification = findViewById(R.id.createNotificationButton);
        createNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNotification();
            }
        });

        Button createNotificationWithIntent = findViewById(R.id.createNotificationWithIntentButton);
        createNotificationWithIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNotificationWithIntent();
            }
        });

        Button createCustomNotification = findViewById(R.id.createCustomNotificationButton);
        createCustomNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCustomNotification();
            }
        });

        Button closeNotification = findViewById(R.id.closeNotificationButton);
        closeNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationManager.cancel(NOTIFY_ID - 1);
            }
        });

        Button closeNotifications = findViewById(R.id.closeNotificationsButton);
        closeNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationManager.cancelAll();
            }
        });
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL,
                    "My notification channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription("My channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void createNotification() {
        String bigText = "dsalkfaksjglajsgkhsakgljhaskfhgkjafghasdkghkdsahgkghsdakghaskdjsdhgkalshgksagkhdsakgjhsakgh" +
                "lkdsjfkasjdlagslkjsaglkjsalkghlkasdhgklhsagkhsadkghaskdghksalkcsamlksajkjaesfmn,mandflasdfkajdslfjas" +
                "kjdsahfkjsahdkfhsadkhfksahdfkhsadkfhaskldhfkashflkashdflkashflksahfkashflsahdlfhasdhfkashflkashflksaj";
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_ggggg, options);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        for (int i = 0; i < 5; i++) {
            inboxStyle.addLine("Line " + i);
        }

        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle("You");
        messagingStyle.setConversationTitle("Chat");
        messagingStyle.addMessage("Hello", System.currentTimeMillis(), "He");
        messagingStyle.addMessage("How are you?", System.currentTimeMillis(), "She");
        messagingStyle.addMessage("Fine", System.currentTimeMillis(), "You");
        messagingStyle.addMessage("Well", System.currentTimeMillis(), "He");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL);
        builder.setSmallIcon(R.drawable.ic_person);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person));
//        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(bigText));
//        builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
//        builder.setStyle(inboxStyle);
        builder.setStyle(messagingStyle);

        if (NOTIFY_ID - 100 < titles.length) {
            builder.setContentTitle(titles[NOTIFY_ID - 100]);
            builder.setContentText(notifications[NOTIFY_ID - 100]);

            Notification notification = builder.build();
            notificationManager.notify(NOTIFY_ID, notification);
            NOTIFY_ID++;
        }
    }

    private void createNotificationWithIntent() {
        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_ggggg);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_ggggg));
        if (NOTIFY_WITH_INTENT_ID - 200 < titles.length) {
            builder.setContentTitle(titles[NOTIFY_WITH_INTENT_ID - 200]);
            builder.setContentText(notifications[NOTIFY_WITH_INTENT_ID - 200]);
            builder.setAutoCancel(true);

            Notification notification = builder.build();
            notificationManager.notify(NOTIFY_WITH_INTENT_ID, notification);
            NOTIFY_WITH_INTENT_ID++;
        }
    }

    private void createCustomNotification() {
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                MainActivity.this,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.layout_notification);
        remoteViews.setTextViewText(R.id.notificationTextView, "Out custom text");
        remoteViews.setOnClickPendingIntent(R.id.root, pendingIntent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL);
        builder.setSmallIcon(R.drawable.ic_ggggg);
        builder.setAutoCancel(true);
        builder.setContent(remoteViews);

        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_CUSTOM_ID, notification);
    }
}